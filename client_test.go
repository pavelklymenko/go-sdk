package sdk

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"net/http/httptest"
	"reflect"
	"sort"
	"strings"
	"testing"
)

var emptyAPIErr *APIError

type errorRouteHandler func(*testing.T, *Client) (*APIError, error)

var (
	getProductData errorRouteHandler = func(t *testing.T, c *Client) (*APIError, error) {
		resp, apiErr, err := c.GetProduct(42)
		var emptyResp *Product
		assert(t, resp, emptyResp)
		return apiErr, err
	}
	addRating errorRouteHandler = func(t *testing.T, c *Client) (*APIError, error) {
		success, apiErr, err := c.AddRating(42, "foo@paddle.com", "ABCD", "Windows", "10", "2.1", "Patch 22", 5)
		assert(t, false, success)
		return apiErr, err
	}
	verifyLicense errorRouteHandler = func(t *testing.T, c *Client) (*APIError, error) {
		success, apiErr, err := c.VerifyLicense("FOO", 42)
		assert(t, success, false)
		return apiErr, err
	}
	verifyLicenseWithUUID errorRouteHandler = func(t *testing.T, c *Client) (*APIError, error) {
		success, apiErr, err := c.VerifyLicenseWithUUID("FOO", 42, "BAZ")
		assert(t, success, false)
		return apiErr, err
	}
	activateLicense errorRouteHandler = func(t *testing.T, c *Client) (*APIError, error) {
		activationID, apiErr, err := c.ActivateLicense(42, "foo@paddle.com", "FOO", "ABCD1")
		assert(t, ActivationID(0), activationID)
		return apiErr, err
	}
	deactivateLicense errorRouteHandler = func(t *testing.T, c *Client) (*APIError, error) {
		success, apiErr, err := c.DeactivateLicense(42)
		assert(t, false, success)
		return apiErr, err
	}
	getPurchaseIDS errorRouteHandler = func(t *testing.T, c *Client) (*APIError, error) {
		purchaseIds, apiErr, err := c.GetPurchaseIDs(42)
		var emptyPurchaseIDs []PurchaseID
		assert(t, emptyPurchaseIDs, purchaseIds)
		return apiErr, err
	}
	getPurchases errorRouteHandler = func(t *testing.T, c *Client) (*APIError, error) {
		purchases, apiErr, err := c.GetPurchases(42)
		var emptyPurchases map[PurchaseID]*Purchase
		assert(t, emptyPurchases, purchases)
		return apiErr, err
	}
	getPurchase errorRouteHandler = func(t *testing.T, c *Client) (*APIError, error) {
		purchase, apiErr, err := c.GetPurchase("42.1")
		var emptyPurchase *Purchase
		assert(t, emptyPurchase, purchase)
		return apiErr, err
	}
	activatePurchase errorRouteHandler = func(t *testing.T, c *Client) (*APIError, error) {
		activatedPurch, apiErr, err := c.ActivatePurchase("12.1", "TOKEN", "APP1")
		var emptyActivatedPurch *ActivatedPurchase
		assert(t, emptyActivatedPurch, activatedPurch)
		return apiErr, err
	}
	verifyPurchase errorRouteHandler = func(t *testing.T, c *Client) (*APIError, error) {
		success, apiErr, err := c.VerifyPurchase("12.1", "TOKEN", "APP1")
		assert(t, false, success)
		return apiErr, err
	}
	addToMailingList errorRouteHandler = func(t *testing.T, c *Client) (*APIError, error) {
		success, apiErr, err := c.AddToMailingList(42, "foo@paddle.com", "ABCD", "Windows", "10", "2.1", "Patch 22")
		assert(t, false, success)
		return apiErr, err
	}
	sendFeedback errorRouteHandler = func(t *testing.T, c *Client) (*APIError, error) {
		success, apiErr, err := c.SendFeedback(42, "foo@paddle.com", "ABCD", "Windows", "10", "2.1", "Patch 22", "Foo", "bug", "foobar")
		assert(t, false, success)
		return apiErr, err
	}
	addHappinessRating errorRouteHandler = func(t *testing.T, c *Client) (*APIError, error) {
		success, apiErr, err := c.AddHappinessRating(42, "foo@paddle.com", "ABCD", "Windows", "10", "2.1", "Patch 22", Happy)
		assert(t, false, success)
		return apiErr, err
	}
)

func Test_getProductDataReturnsProductData(t *testing.T) {
	httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, `{
	  	"success": true,
	  	"response": {
	  		"name": "553a5f5e8c5c5",
	  		"image": "http://example.com/553a5f5e93be8.png",
	  		"current_prices": {
	  			"USD": 4
	  		},
	  		"checkout_secret_key": "tx9bv5",
	  		"base_prices": {
	  			"USD": 10
	  		},
	  		"discount_percentage": {
	  			"USD": 60
	  		},
	  		"discount_value": {
	  			"USD": 6
	  		},
	  		"vendor_name": "553a5f5e50fc6",
	  		"payment_url": "https://pay.paddle.com/checkout/framework/1199"
	  	}
	  }`)
	})
	defer s.Close()

	c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
	resp, apiErr, err := c.GetProduct(42)
	assert(t, err, nil)
	assert(t, apiErr, emptyAPIErr)
	assert(t, resp, &Product{
		Name:                "553a5f5e8c5c5",
		Image:               "http://example.com/553a5f5e93be8.png",
		CurrentPrices:       map[string]float64{"USD": 4},
		SecretCheckoutKey:   "tx9bv5",
		BasePrices:          map[string]float64{"USD": 10},
		DiscountPercentages: map[string]float64{"USD": 60},
		DiscountValue:       map[string]float64{"USD": 6},
		VendorName:          "553a5f5e50fc6",
		PaymentURL:          "https://pay.paddle.com/checkout/framework/1199",
	})
}

func Test_getProductDataHandlesStringPrices(t *testing.T) {
	httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, `{
	  	"success": true,
	  	"response": {
	  		"name": "553a5f5e8c5c5",
	  		"image": "http://example.com/553a5f5e93be8.png",
	  		"current_prices": {
	  			"USD": "4"
	  		},
	  		"checkout_secret_key": "tx9bv5",
	  		"base_prices": {
	  			"USD": "10"
	  		},
	  		"discount_percentage": {
	  			"USD": "60"
	  		},
	  		"discount_value": {
	  			"USD": "6"
	  		},
	  		"vendor_name": "553a5f5e50fc6",
	  		"payment_url": "https://pay.paddle.com/checkout/framework/1199"
	  	}
	  }`)
	})
	defer s.Close()

	c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
	resp, apiErr, err := c.GetProduct(42)
	assert(t, err, nil)
	assert(t, apiErr, emptyAPIErr)
	assert(t, resp, &Product{
		Name:                "553a5f5e8c5c5",
		Image:               "http://example.com/553a5f5e93be8.png",
		CurrentPrices:       map[string]float64{"USD": 4},
		SecretCheckoutKey:   "tx9bv5",
		BasePrices:          map[string]float64{"USD": 10},
		DiscountPercentages: map[string]float64{"USD": 60},
		DiscountValue:       map[string]float64{"USD": 6},
		VendorName:          "553a5f5e50fc6",
		PaymentURL:          "https://pay.paddle.com/checkout/framework/1199",
	})
}

func Test_getProductDataReturnsApiError(t *testing.T) {
	assertRouteReturnsAPIError(t, getProductData)
}

func Test_getProductDataReturnsGenericError(t *testing.T) {
	assertRouteHandlesInvalidJSON(t, getProductData)
}

func Test_verifyLicenseReturnsSuccesfulResult(t *testing.T) {
	testResultOfVerifyLicense(t, func(c *Client) (bool, *APIError, error) {
		return c.VerifyLicense("BAR", 42)
	}, true)
}

func Test_verifyLicenseReturnsUnsuccesfulResult(t *testing.T) {
	testResultOfVerifyLicense(t, func(c *Client) (bool, *APIError, error) {
		return c.VerifyLicense("BAR", 42)
	}, false)
}

func Test_verifyLicenseReturnsApiError(t *testing.T) {
	assertRouteReturnsAPIError(t, verifyLicense)
}

func Test_verifyLicenseReturnsGenericError(t *testing.T) {
	assertRouteHandlesInvalidJSON(t, verifyLicense)
}

func Test_verifyLicenseWithUUIDReturnsSuccesfulResult(t *testing.T) {
	testResultOfVerifyLicense(t, func(c *Client) (bool, *APIError, error) {
		return c.VerifyLicenseWithUUID("BAR", 42, "FOO")
	}, true)
}

func Test_verifyLicenseWithUUIDReturnsUnsuccesfulResult(t *testing.T) {
	testResultOfVerifyLicense(t, func(c *Client) (bool, *APIError, error) {
		return c.VerifyLicenseWithUUID("BAR", 42, "FOO")
	}, false)
}

func Test_verifyLicenseWithUUIDReturnsApiError(t *testing.T) {
	assertRouteReturnsAPIError(t, verifyLicenseWithUUID)
}

func Test_verifyLicenseWithUUIDReturnsGenericError(t *testing.T) {
	assertRouteHandlesInvalidJSON(t, verifyLicenseWithUUID)
}

func Test_activateLicenseReturnsActivationID(t *testing.T) {
	httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, `{"success":true,"response":{"activation_id":55}}`)
	})
	defer s.Close()

	c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
	success, apiErr, err := c.ActivateLicense(42, "foo@paddle.com", "BAR", "12DSF-AB973B")
	assert(t, nil, err)
	assert(t, emptyAPIErr, apiErr)
	assert(t, ActivationID(55), success)
}

func Test_activateLicenseReturnsApiError(t *testing.T) {
	assertRouteReturnsAPIError(t, activateLicense)
}

func Test_activateLicenseReturnsGenericError(t *testing.T) {
	assertRouteHandlesInvalidJSON(t, activateLicense)
}

func Test_deactivateLicenseReturnsSuccessfulResult(t *testing.T) {
	httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, `{"success":true,"response":""}`)
	})
	defer s.Close()

	c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
	success, apiErr, err := c.DeactivateLicense(42)
	assert(t, err, nil)
	assert(t, apiErr, emptyAPIErr)
	assert(t, success, true)
}

func Test_deactivateLicenseReturnsApiError(t *testing.T) {
	assertRouteReturnsAPIError(t, deactivateLicense)
}

func Test_deactivateLicenseReturnsGenericError(t *testing.T) {
	assertRouteHandlesInvalidJSON(t, deactivateLicense)
}

func Test_getPurchaseIDsReturnsListOfPurchaseIDs(t *testing.T) {
	httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, `{"success":true,"response":{"1121.1":[],"1121.3":[]}}`)
	})
	defer s.Close()

	c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
	purchaseIDs, apiErr, err := c.GetPurchaseIDs(42)
	assert(t, err, nil)
	assert(t, apiErr, emptyAPIErr)

	// Convert the purchase IDs to a list of strings so that they can be sorted.
	// The requirement of sorting the strings is due to maps being an unordered data
	// structure. Without the sorting then, this test would fail occasionally.
	purchaseIDStrings := make([]string, len(purchaseIDs))
	for index, id := range purchaseIDs {
		purchaseIDStrings[index] = string(id)
	}
	sort.Strings(purchaseIDStrings)
	assert(t, purchaseIDStrings, []string{"1121.1", "1121.3"})
}

func Test_getPurchaseIDsReturnsEmptyListOfPurchaseIDs(t *testing.T) {
	httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, `{"success":true,"response":[]}`)
	})
	defer s.Close()

	c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
	purchaseIDs, apiErr, err := c.GetPurchaseIDs(42)
	assert(t, nil, err)
	assert(t, emptyAPIErr, apiErr)
	var emptyPurchaseIDs []PurchaseID
	assert(t, emptyPurchaseIDs, purchaseIDs)
}

func Test_getPurchaseIDsReturnsApiError(t *testing.T) {
	assertRouteReturnsAPIError(t, getPurchaseIDS)
}

func Test_getPurchaseIDsReturnsGenericError(t *testing.T) {
	assertRouteHandlesInvalidJSON(t, getPurchaseIDS)
}

func Test_getPurchaseReturnsPurchase(t *testing.T) {
	httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, `{
			"success": true,
			"response": {
				"1122.3": {
					"price": 12,
					"currency": "EUR",
					"language": "BEL",
					"name": "FOO",
					"custom_identifier": "BAR",
					"type": "consumable",
					"description": "purchase",
					"created_at": "2015-06-03 17:09:10",
					"updated_at": "2015-06-03 17:09:10",
					"icon": "https://pay.paddle.com/assets/images/checkout/foo.png"
				}
			}
		}`)
	})
	defer s.Close()

	c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
	purchase, apiErr, err := c.GetPurchase("1122.3")
	assert(t, nil, err)
	assert(t, emptyAPIErr, apiErr)

	expectedPurchase := &Purchase{
		Price:            12,
		CurrencyCode:     "EUR",
		Language:         "BEL",
		Name:             "FOO",
		CustomIdentifier: "BAR",
		Type:             "consumable",
		Description:      "purchase",
		Created:          "2015-06-03 17:09:10",
		LastUpdated:      "2015-06-03 17:09:10",
		Icon:             "https://pay.paddle.com/assets/images/checkout/foo.png",
	}
	assert(t, expectedPurchase, purchase)
}

func Test_getPurchaseReturnsNoPurchase(t *testing.T) {
	httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, `{"success":true,"response":[]}`)
	})
	defer s.Close()

	c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
	purchase, apiErr, err := c.GetPurchase("42.1")
	assert(t, nil, err)
	assert(t, emptyAPIErr, apiErr)
	var emptyPurchase *Purchase
	assert(t, emptyPurchase, purchase)
}

func Test_getPurchaseReturnsApiError(t *testing.T) {
	assertRouteReturnsAPIError(t, getPurchase)
}

func Test_getPurchaseReturnsGenericError(t *testing.T) {
	assertRouteHandlesInvalidJSON(t, getPurchase)
}

func Test_getPurchasesReturnsListOfPurchases(t *testing.T) {
	httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, `{
			"success": true,
			"response": {
				"1122.1": {
					"price": 10,
					"currency": "USD",
					"language": "RUS",
					"name": "556dd52702e6a",
					"custom_identifier": "556dd52702f3d",
					"type": "consumable",
					"description": "556dd52702ffe",
					"created_at": "2015-06-02 17:09:10",
					"updated_at": "2015-06-02 17:09:10",
					"icon": "https://pay.paddle.com/assets/images/checkout/default_product_icon.png"
				},
				"1122.3": {
					"price": 12,
					"currency": "EUR",
					"language": "BEL",
					"name": "FOO",
					"custom_identifier": "BAR",
					"type": "consumable",
					"description": "purchase",
					"created_at": "2015-06-03 17:09:10",
					"updated_at": "2015-06-03 17:09:10",
					"icon": "https://pay.paddle.com/assets/images/checkout/foo.png"
				}
			}
		}`)
	})
	defer s.Close()

	c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
	purchases, apiErr, err := c.GetPurchases(42)
	assert(t, nil, err)
	assert(t, emptyAPIErr, apiErr)

	expectedPurchases := map[PurchaseID]*Purchase{
		"1122.1": &Purchase{
			Price:            10,
			CurrencyCode:     "USD",
			Language:         "RUS",
			Name:             "556dd52702e6a",
			CustomIdentifier: "556dd52702f3d",
			Type:             "consumable",
			Description:      "556dd52702ffe",
			Created:          "2015-06-02 17:09:10",
			LastUpdated:      "2015-06-02 17:09:10",
			Icon:             "https://pay.paddle.com/assets/images/checkout/default_product_icon.png",
		},
		"1122.3": &Purchase{
			Price:            12,
			CurrencyCode:     "EUR",
			Language:         "BEL",
			Name:             "FOO",
			CustomIdentifier: "BAR",
			Type:             "consumable",
			Description:      "purchase",
			Created:          "2015-06-03 17:09:10",
			LastUpdated:      "2015-06-03 17:09:10",
			Icon:             "https://pay.paddle.com/assets/images/checkout/foo.png",
		},
	}
	for pID, expectedPurchase := range expectedPurchases {
		assert(t, expectedPurchase, purchases[pID])
	}
}

func Test_getPurchasesHandlesEmptyProperties(t *testing.T) {
	httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, `{
			"success": true,
			"response": {
				"1122.1": {
					"price": 10,
					"currency": "USD",
					"language": null,
					"name": null,
					"custom_identifier": null,
					"type": "consumable",
					"description": null,
					"created_at": "2015-06-02 17:09:10",
					"updated_at": "2015-06-02 17:09:10",
					"icon": "https://pay.paddle.com/assets/images/checkout/default_product_icon.png"
				}
			}
		}`)
	})
	defer s.Close()

	c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
	purchases, apiErr, err := c.GetPurchases(42)
	assert(t, err, nil)
	assert(t, apiErr, emptyAPIErr)

	expectedPurchases := map[PurchaseID]*Purchase{
		"1122.1": &Purchase{
			Price:        10,
			CurrencyCode: "USD",
			Type:         "consumable",
			Created:      "2015-06-02 17:09:10",
			LastUpdated:  "2015-06-02 17:09:10",
			Icon:         "https://pay.paddle.com/assets/images/checkout/default_product_icon.png",
		},
	}
	for pID, expectedPurchase := range expectedPurchases {
		assert(t, expectedPurchase, purchases[pID])
	}
}

func Test_getPurchasesHandlesStringPrice(t *testing.T) {
	httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, `{
			"success": true,
			"response": {
				"1122.1": {
					"price": "10",
					"currency": "USD",
					"language": "RUS",
					"name": "556dd52702e6a",
					"custom_identifier": "556dd52702f3d",
					"type": "consumable",
					"description": "556dd52702ffe",
					"created_at": "2015-06-02 17:09:10",
					"updated_at": "2015-06-02 17:09:10",
					"icon": "https://pay.paddle.com/assets/images/checkout/default_product_icon.png"
				}
			}
		}`)
	})
	defer s.Close()

	c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
	purchases, apiErr, err := c.GetPurchases(42)
	assert(t, err, nil)
	assert(t, apiErr, emptyAPIErr)

	expectedPurchases := map[PurchaseID]*Purchase{
		"1122.1": &Purchase{
			Price:            10,
			CurrencyCode:     "USD",
			Language:         "RUS",
			Name:             "556dd52702e6a",
			CustomIdentifier: "556dd52702f3d",
			Type:             "consumable",
			Description:      "556dd52702ffe",
			Created:          "2015-06-02 17:09:10",
			LastUpdated:      "2015-06-02 17:09:10",
			Icon:             "https://pay.paddle.com/assets/images/checkout/default_product_icon.png",
		},
	}
	for pID, expectedPurchase := range expectedPurchases {
		assert(t, expectedPurchase, purchases[pID])
	}
}

func Test_getPurchasesReturnsEmptyListOfPurchases(t *testing.T) {
	httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, `{"success":true,"response":[]}`)
	})
	defer s.Close()

	c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
	purchases, apiErr, err := c.GetPurchases(42)
	assert(t, nil, err)
	assert(t, emptyAPIErr, apiErr)
	var emptyPurchases map[PurchaseID]*Purchase
	assert(t, emptyPurchases, purchases)
}

func Test_getPurchasesReturnsApiError(t *testing.T) {
	assertRouteReturnsAPIError(t, getPurchases)
}

func Test_getPurchasesReturnsGenericError(t *testing.T) {
	assertRouteHandlesInvalidJSON(t, getPurchases)
}

func Test_activatePurchaseReturnsActivatedPurchase(t *testing.T) {
	httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, `{"success":true,"response":{"token":"FOO","user_id":42,"user_email":"foo@paddle.com"}}`)
	})
	defer s.Close()

	c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
	activatedPurchase, apiErr, err := c.ActivatePurchase("12.1", "ABCD", "APP1")
	assert(t, err, nil)
	assert(t, apiErr, emptyAPIErr)
	assert(t, activatedPurchase, &ActivatedPurchase{
		Token:     "FOO",
		UserID:    42,
		UserEmail: "foo@paddle.com",
	})
}

func Test_activatePurchaseReturnsApiError(t *testing.T) {
	assertRouteReturnsAPIError(t, activatePurchase)
}

func Test_activatePurchaseReturnsGenericError(t *testing.T) {
	assertRouteHandlesInvalidJSON(t, activatePurchase)
}

func Test_verifyPurchaseReturnsSuccessfulResult(t *testing.T) {
	httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, `{"success":true,"response":{"message":"Token Verified"}}`)
	})
	defer s.Close()

	c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
	success, apiErr, err := c.VerifyPurchase("12.1", "FOO", "APP1")
	assert(t, err, nil)
	assert(t, apiErr, emptyAPIErr)
	assert(t, success, true)
}

func Test_verifyPurchaseReturnsApiError(t *testing.T) {
	assertRouteReturnsAPIError(t, verifyPurchase)
}

func Test_verifyPurchaseReturnsGenericError(t *testing.T) {
	assertRouteHandlesInvalidJSON(t, verifyPurchase)
}

func Test_addToMailingListReturnsSuccessfulResult(t *testing.T) {
	httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, `{"success":true,"response":""}`)
	})
	defer s.Close()

	c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
	success, apiErr, err := c.AddToMailingList(42, "foo@paddle.com", "ABCD", "Windows", "10", "2.1", "Patch 22")
	assert(t, err, nil)
	assert(t, apiErr, emptyAPIErr)
	assert(t, success, true)
}

func Test_addToMailingListReturnsApiError(t *testing.T) {
	assertRouteReturnsAPIError(t, addToMailingList)
}

func Test_addToMailingListReturnsGenericError(t *testing.T) {
	assertRouteHandlesInvalidJSON(t, addToMailingList)
}

func Test_sendFeedbackReturnsSuccessfulResult(t *testing.T) {
	httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, `{"success":true,"response":""}`)
	})
	defer s.Close()

	c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
	success, apiErr, err := c.SendFeedback(42, "foo@paddle.com", "ABCD", "Windows", "10", "2.1", "Patch 22", "Foo", "bug", "foobar")
	assert(t, err, nil)
	assert(t, apiErr, emptyAPIErr)
	assert(t, success, true)
}

func Test_sendFeedbackReturnsApiError(t *testing.T) {
	assertRouteReturnsAPIError(t, sendFeedback)
}

func Test_sendFeedbackReturnsGenericError(t *testing.T) {
	assertRouteHandlesInvalidJSON(t, sendFeedback)
}

func Test_addHappinessRatingReturnsSuccessfulResult(t *testing.T) {
	for _, rating := range []Happiness{Happy, Neutral, Unhappy} {
		httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/json")
			fmt.Fprint(w, `{"success":true,"response":""}`)
		})
		defer s.Close()

		c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
		success, apiErr, err := c.AddHappinessRating(42, "foo@paddle.com", "ABCD", "Windows", "10", "2.1", "Patch 22", rating)
		assert(t, err, nil)
		assert(t, apiErr, emptyAPIErr)
		assert(t, success, true)
	}
}

func Test_addHappinessRatingOnlyAcceptsHappinessConstants(t *testing.T) {
	for _, rating := range []Happiness{0, 4} {
		httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {})
		defer s.Close()

		c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
		success, apiErr, err := c.AddHappinessRating(42, "foo@paddle.com", "ABCD", "Windows", "10", "2.1", "Patch 22", rating)
		assert(t, apiErr, emptyAPIErr)
		assert(t, success, false)
		assert(t, err.Error(), "Happiness rating must be one of the defined happiness rating constants")
	}
}

func Test_addHappinessRatingReturnsApiError(t *testing.T) {
	assertRouteReturnsAPIError(t, addHappinessRating)
}

func Test_addHappinessRatingReturnsGenericError(t *testing.T) {
	assertRouteHandlesInvalidJSON(t, addHappinessRating)
}

func Test_addRatingReturnsSuccessfulResult(t *testing.T) {
	for _, rating := range []uint{1, 2, 3, 4, 5} {
		httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/json")
			fmt.Fprint(w, `{"success":true,"response":""}`)
		})
		defer s.Close()

		c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
		success, apiErr, err := c.AddRating(42, "foo@paddle.com", "ABCD", "Windows", "10", "2.1", "Patch 22", rating)
		assert(t, err, nil)
		assert(t, apiErr, emptyAPIErr)
		assert(t, success, true)
	}
}

func Test_addRatingOnlyAcceptsRatingsBetween1And5(t *testing.T) {
	for _, rating := range []uint{0, 6} {
		httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {})
		defer s.Close()

		c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
		success, apiErr, err := c.AddRating(42, "foo@paddle.com", "ABCD", "Windows", "10", "2.1", "Patch 22", rating)
		assert(t, apiErr, emptyAPIErr)
		assert(t, success, false)
		assert(t, err.Error(), "Rating must be between 1 and 5")
	}
}

func Test_addRatingReturnsApiError(t *testing.T) {
	assertRouteReturnsAPIError(t, addRating)
}

func Test_addRatingReturnsGenericError(t *testing.T) {
	assertRouteHandlesInvalidJSON(t, addRating)
}

func testResultOfVerifyLicense(t *testing.T, verify func(c *Client) (bool, *APIError, error), result bool) {
	httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		if result {
			fmt.Fprint(w, `{"success":true,"response":""}`)
		} else {
			fmt.Fprint(w, `{"success":false,"response":""}`)
		}
	})
	defer s.Close()

	c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
	success, apiErr, err := verify(c)
	assert(t, err, nil)
	assert(t, apiErr, emptyAPIErr)
	assert(t, success, result)
}

func assert(t *testing.T, expected interface{}, actual interface{}) {
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("Expected %+v (type %v) - Got %+v (type %v)",
			expected, reflect.TypeOf(expected),
			actual, reflect.TypeOf(actual),
		)
	}
}

func assertRouteReturnsAPIError(t *testing.T, route errorRouteHandler) {
	httpClient, s, expectedAPIErr := mockNoPermissionAPIError()
	defer s.Close()

	c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
	apiErr, err := route(t, c)
	assert(t, nil, err)
	assert(t, apiErr, expectedAPIErr)
}

func assertRouteHandlesInvalidJSON(t *testing.T, route errorRouteHandler) {
	httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, `<html>`)
	})
	defer s.Close()

	c := &Client{Key: "FOO", HTTPClient: httpClient, BaseURL: DefaultBaseURL}
	apiErr, err := route(t, c)
	assert(t, emptyAPIErr, apiErr)
	assert(t, reflect.TypeOf(&json.SyntaxError{}), reflect.TypeOf(err))
}

func mockNoPermissionAPIError() (*http.Client, *httptest.Server, *APIError) {
	httpClient, s := mockServer(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, `{"success":false,"error":{"code":107,"message":"You don't have permission to access this resource"}}`)
	})
	return httpClient, s, &APIError{Code: 107, Message: "You don't have permission to access this resource"}
}

func mockServer(handler http.HandlerFunc) (*http.Client, *httptest.Server) {
	server := httptest.NewTLSServer(http.HandlerFunc(handler))
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
		Dial: func(network, addr string) (net.Conn, error) {
			return net.Dial("tcp", server.URL[strings.LastIndex(server.URL, "/")+1:])
		},
	}
	return &http.Client{Transport: tr}, server
}
