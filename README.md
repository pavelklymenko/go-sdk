# Paddle SDK client

## Installation

1. Run `go get bitbucket.org/paddlehq/go-sdk`
2. Import `"github.com/paddlehq/go-sdk"`
3. Create a new client using `client := sdk.NewClient("<your-api-key>")`
4. Use the client `client.GetPurchases(<productID>)`

## TODO

1. DONE - Add in all API routes handling only success cases
2. DONE - Test all success cases
3. DONE - Handle API error cases
4. DONE - Test all API error cases
5. DONE - Test non-API errors
6. Test URLs of the API
7. Test form values sent to the API
8. Test success value
9. DONE Test against real API
