package sdk

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

// DefaultBaseURL is the base URL used by NewClient.
const DefaultBaseURL = "https://v3.paddleapi.com/3.0/"

// Client sends payloads to the SDK API and interprets them.
type Client struct {
	Key        string
	BaseURL    string
	HTTPClient *http.Client
}

// APIError represents an error returned from the SDK API
type APIError struct {
	Code    uint
	Message string
}

// Product represents the product data as returned from `product/data`.
type Product struct {
	Name                string
	Image               string
	CurrentPrices       map[string]float64
	SecretCheckoutKey   string
	BasePrices          map[string]float64
	DiscountPercentages map[string]float64
	DiscountValue       map[string]float64
	VendorName          string
	PaymentURL          string
}

// ActivationID represents a reference to the activated license
type ActivationID uint

// PurchaseID represents the ID of the product purchase.
type PurchaseID string

// Purchase represents a purchase of a product.
type Purchase struct {
	Price            float64
	CurrencyCode     string
	Language         string
	Name             string
	CustomIdentifier string
	Type             string
	Description      string
	Created          string
	LastUpdated      string
	Icon             string
}

// purchaseResponse represents a purchase as it is returned from the API. Both GetPurchases and GetPurchase rely
// on this type and convert it to the actual purchase type, Purchase.
type purchaseResponse struct {
	Price            interface{}
	CurrencyCode     string `json:"currency"`
	Language         string `json:",omitempty"`
	Name             string `json:",omitempty"`
	CustomIdentifier string `json:"custom_identifier,omitempty"`
	Type             string
	Description      string `json:",omitempty"`
	Created          string `json:"created_at"`
	LastUpdated      string `json:"updated_at"`
	Icon             string
}

// PurchaseToken is the token returned when a purchase is activated. It can be used
// to verify that the purchase is legitimate.
type PurchaseToken string

// ActivatedPurchase - TODO what is it used for?
type ActivatedPurchase struct {
	Token     PurchaseToken
	UserID    uint
	UserEmail string
}

// Happiness represents an emotional rating the user has given for the app in terms of happiness
type Happiness uint

const (
	// Happy represents a positive rating of the app
	Happy Happiness = 1
	// Neutral represents a neutral rating of the app
	Neutral Happiness = 2
	// Unhappy represents a negative rating of the app
	Unhappy Happiness = 3
)

// apiResponse is an intermediate type to match API error responses.
type apiResponse struct {
	Success bool
	Err     *APIError `json:"error"`
}

// NewClient creates a new client with the given key
func NewClient(key string) *Client {
	return &Client{Key: key, BaseURL: DefaultBaseURL, HTTPClient: http.DefaultClient}
}

// GetProduct returns information about one of your products.
func (c *Client) GetProduct(productID uint32) (*Product, *APIError, error) {
	values := map[string][]string{"product_id": {fmt.Sprintf("%d", productID)}}
	// values.Set("product_id", string(productID))
	var cont struct {
		Success  bool
		Response *struct {
			Name                string
			Image               string
			CurrentPrices       map[string]interface{} `json:"current_prices"`
			SecretCheckoutKey   string                 `json:"checkout_secret_key"`
			BasePrices          map[string]interface{} `json:"base_prices"`
			DiscountPercentages map[string]interface{} `json:"discount_percentage"`
			DiscountValue       map[string]interface{} `json:"discount_value"`
			VendorName          string                 `json:"vendor_name"`
			PaymentURL          string                 `json:"payment_url"`
		}
		Err *APIError `json:"error"`
	}
	if _, err := c.makeRequest("product/data", values, &cont); err != nil {
		return nil, nil, err
	}
	if cont.Err != nil {
		return nil, cont.Err, nil
	}

	// Convert the prices from string|float64 to float64. If unable to convert, return an error.
	currPrices, err := convertPricesToFloat(cont.Response.CurrentPrices)
	if err != nil {
		return nil, nil, err
	}
	basePrices, err := convertPricesToFloat(cont.Response.BasePrices)
	if err != nil {
		return nil, nil, err
	}
	discountPercs, err := convertPricesToFloat(cont.Response.DiscountPercentages)
	if err != nil {
		return nil, nil, err
	}
	discountVals, err := convertPricesToFloat(cont.Response.DiscountValue)
	if err != nil {
		return nil, nil, err
	}

	return &Product{
		Name:                cont.Response.Name,
		Image:               cont.Response.Image,
		SecretCheckoutKey:   cont.Response.SecretCheckoutKey,
		VendorName:          cont.Response.VendorName,
		PaymentURL:          cont.Response.PaymentURL,
		CurrentPrices:       currPrices,
		BasePrices:          basePrices,
		DiscountPercentages: discountPercs,
		DiscountValue:       discountVals,
	}, nil, nil
}

// VerifyLicense verifies that the license is active.
func (c *Client) VerifyLicense(licenseKey string, activationID ActivationID) (bool, *APIError, error) {
	return c.verifyLicense(licenseKey, activationID, url.Values(make(map[string][]string)))
}

// VerifyLicenseWithUUID verifies that the license is active, and that either the UUID matches the UUID of the
// activation or that the UUID is not set.
func (c *Client) VerifyLicenseWithUUID(licenseKey string, activationID ActivationID, uuid string) (bool, *APIError, error) {
	values := url.Values(make(map[string][]string))
	values.Add("uuid", uuid)

	return c.verifyLicense(licenseKey, activationID, values)
}

// verifyLicense makes the request to `license/verify` with any additional form values specified in values.
func (c *Client) verifyLicense(licenseKey string, activationID ActivationID, values url.Values) (bool, *APIError, error) {
	values.Add("license_key", licenseKey)
	values.Add("activation_id", fmt.Sprintf("%d", activationID))
	return c.handleBoolResponse("license/verify", values)
}

// ActivateLicense activates a license
func (c *Client) ActivateLicense(productID uint, email, licenseKey, uuid string) (ActivationID, *APIError, error) {
	values := url.Values(make(map[string][]string))
	values.Add("email", email)
	values.Add("license_key", licenseKey)
	values.Add("uuid", uuid)
	values.Add("product_id", fmt.Sprintf("%d", productID))
	var resp struct {
		Success  bool
		Response *struct {
			ActivationID interface{} `json:"activation_id"`
		}
		Err *APIError `json:"error"`
	}
	if _, err := c.makeRequest("license/activate", values, &resp); err != nil {
		return 0, nil, err
	}
	if resp.Err != nil {
		return 0, resp.Err, nil
	}
	if resp.Response != nil {
		id, err := idToUint(resp.Response.ActivationID)
		if err != nil {
			return 0, nil, err
		}
		return ActivationID(id), nil, nil
	}
	return 0, nil, errors.New("No activation ID found")
}

// DeactivateLicense deactivates a license using the activation ID
func (c *Client) DeactivateLicense(id ActivationID) (bool, *APIError, error) {
	return c.handleBoolResponse("license/deactivate", map[string][]string{"activation_id": {fmt.Sprintf("%d", id)}})
}

// GetPurchaseIDs returns a list of the product purchase IDs (<prodID>.<purchID>)
// of the product.
func (c *Client) GetPurchaseIDs(productID uint) ([]PurchaseID, *APIError, error) {
	values := url.Values(make(map[string][]string))
	values.Add("product_id", fmt.Sprintf("%d", productID))
	values.Add("details", "0")
	var resp struct {
		Success  bool
		Response interface{}
		Err      *APIError `json:"error"`
	}
	if _, err := c.makeRequest("purchase/collection", values, &resp); err != nil {
		return nil, nil, err
	}
	if resp.Err != nil {
		return nil, resp.Err, nil
	}

	switch purchaseIds := resp.Response.(type) {
	case map[string]interface{}:
		prodPurchaseIDs := make([]PurchaseID, len(purchaseIds))
		i := 0
		for id := range purchaseIds {
			prodPurchaseIDs[i] = PurchaseID(id)
			i++
		}
		return prodPurchaseIDs, nil, nil
	case []interface{}:
		return nil, nil, nil
	default:
		return nil, nil, errors.New("Unable to extract purchase IDs from API response")
	}
}

// GetPurchases returns the purchases of the product including details for each purchase.
func (c *Client) GetPurchases(productID uint) (map[PurchaseID]*Purchase, *APIError, error) {
	values := url.Values(make(map[string][]string))
	values.Add("product_id", fmt.Sprintf("%d", productID))
	values.Add("details", "1")
	var resp struct {
		Success  bool
		Response map[PurchaseID]*purchaseResponse
		Err      *APIError `json:"error"`
	}
	if body, err := c.makeRequest("purchase/collection", values, &resp); err != nil {
		// It's possible that the response is an empty array (`[]`). In this case we can still
		// try to match the response against that type.
		var emptyResponse struct {
			Success  bool
			Response []interface{}
		}
		if err := json.Unmarshal(body, &emptyResponse); err == nil {
			return nil, nil, nil
		}

		return nil, nil, err
	}
	if resp.Err != nil {
		return nil, resp.Err, nil
	}

	prodPurchases := make(map[PurchaseID]*Purchase, len(resp.Response))
	for prodPurchID, responsePurchase := range resp.Response {
		purchase, err := responsePurchase.createPurchaseFromPurchaseResponse()
		if err != nil {
			return nil, nil, err
		}
		prodPurchases[prodPurchID] = purchase
	}
	return prodPurchases, nil, nil
}

func (rp *purchaseResponse) createPurchaseFromPurchaseResponse() (*Purchase, error) {
	pr, err := priceToFloat(rp.Price)
	if err != nil {
		return nil, err
	}
	return &Purchase{
		Price:            pr,
		CurrencyCode:     rp.CurrencyCode,
		Language:         rp.Language,
		Name:             rp.Name,
		CustomIdentifier: rp.CustomIdentifier,
		Type:             rp.Type,
		Description:      rp.Description,
		Created:          rp.Created,
		LastUpdated:      rp.LastUpdated,
		Icon:             rp.Icon,
	}, nil
}

// GetPurchase returns a single product purchase.
// TODO test
func (c *Client) GetPurchase(id PurchaseID) (*Purchase, *APIError, error) {
	values := map[string][]string{"purchase_identifier": {string(id)}}
	var resp struct {
		Success  bool
		Response map[PurchaseID]*purchaseResponse
		Err      *APIError `json:"error"`
	}
	if body, err := c.makeRequest("purchase/details", values, &resp); err != nil {
		// It's possible that the response is an empty array (`[]`). In this case we can still
		// try to match the response against that type.
		var emptyResponse struct {
			Success  bool
			Response []interface{}
		}
		if err := json.Unmarshal(body, &emptyResponse); err == nil {
			return nil, nil, nil
		}

		return nil, nil, err
	}
	if resp.Err != nil {
		return nil, resp.Err, nil
	}

	prodPurchases := make(map[PurchaseID]*Purchase, 1)
	for prodPurchID, responsePurchase := range resp.Response {
		purchase, err := responsePurchase.createPurchaseFromPurchaseResponse()
		if err != nil {
			return nil, nil, err
		}
		prodPurchases[prodPurchID] = purchase
		break
	}
	return prodPurchases[id], nil, nil
}

// ActivatePurchase activates a product purchase
func (c *Client) ActivatePurchase(id PurchaseID, activationToken, appID string) (*ActivatedPurchase, *APIError, error) {
	values := url.Values(make(map[string][]string))
	values.Add("purchase_identifier", string(id))
	values.Add("token", activationToken)
	values.Add("app_identifier", appID)
	var resp struct {
		Success  bool
		Response *struct {
			Token     PurchaseToken
			UserID    interface{} `json:"user_id"`
			UserEmail string      `json:"user_email"`
		}
		Err *APIError `json:"error"`
	}
	if _, err := c.makeRequest("purchase/activate", values, &resp); err != nil {
		return nil, nil, err
	}
	if resp.Err != nil {
		return nil, resp.Err, nil
	}

	userID, err := idToUint(resp.Response.UserID)
	if err != nil {
		return nil, nil, err
	}
	return &ActivatedPurchase{
		Token:     resp.Response.Token,
		UserID:    userID,
		UserEmail: resp.Response.UserEmail,
	}, nil, nil
}

// VerifyPurchase verifies that the purchase is legitimate
func (c *Client) VerifyPurchase(id PurchaseID, token PurchaseToken, appID string) (bool, *APIError, error) {
	values := url.Values(make(map[string][]string))
	values.Add("purchase_identifier", string(id))
	values.Add("token", string(token))
	values.Add("app_identifier", appID)
	return c.handleBoolResponse("purchase/verify", values)
}

// AddToMailingList adds an email to the mailing list
func (c *Client) AddToMailingList(productID uint, email, systemUUID, systemOS, systemOSVersion, systemAppVersion, systemOSBuild string) (bool, *APIError, error) {
	values := url.Values(make(map[string][]string))
	values.Add("product_id", fmt.Sprintf("%d", productID))
	values.Add("email", email)
	values.Add("system_uuid", systemUUID)
	values.Add("system_os", systemOS)
	values.Add("system_os_version", systemOSVersion)
	values.Add("system_os_build", systemOSBuild)
	values.Add("system_app_version", systemAppVersion)
	return c.handleBoolResponse("growkit/email", values)
}

// SendFeedback submits feedback of the user
func (c *Client) SendFeedback(productID uint, email, systemUUID, systemOS, systemOSVersion, systemAppVersion, systemOSBuild, name, label, content string) (bool, *APIError, error) {
	values := url.Values(make(map[string][]string))
	values.Add("product_id", fmt.Sprintf("%d", productID))
	values.Add("email", email)
	values.Add("system_uuid", systemUUID)
	values.Add("system_os", systemOS)
	values.Add("system_os_version", systemOSVersion)
	values.Add("system_os_build", systemOSBuild)
	values.Add("system_app_version", systemAppVersion)
	values.Add("name", name)
	values.Add("label", label)
	values.Add("content", content)
	return c.handleBoolResponse("growkit/feedback", values)
}

// AddHappinessRating submits a happiness rating of the user.
func (c *Client) AddHappinessRating(productID uint, email, systemUUID, systemOS, systemOSVersion, systemAppVersion, systemOSBuild string, happiness Happiness) (bool, *APIError, error) {
	if happiness < Happy || happiness > Unhappy {
		return false, nil, errors.New("Happiness rating must be one of the defined happiness rating constants")
	}

	values := url.Values(make(map[string][]string))
	values.Add("product_id", fmt.Sprintf("%d", productID))
	values.Add("email", email)
	values.Add("system_uuid", systemUUID)
	values.Add("system_os", systemOS)
	values.Add("system_os_version", systemOSVersion)
	values.Add("system_os_build", systemOSBuild)
	values.Add("system_app_version", systemAppVersion)
	values.Add("happiness", fmt.Sprintf("%d", happiness))
	return c.handleBoolResponse("growkit/happiness", values)
}

// AddRating adds a rating of the app
func (c *Client) AddRating(productID uint, email, systemUUID, systemOS, systemOSVersion, systemAppVersion, systemOSBuild string, rating uint) (bool, *APIError, error) {
	if rating < 1 || rating > 5 {
		return false, nil, errors.New("Rating must be between 1 and 5")
	}

	values := url.Values(make(map[string][]string))
	values.Add("product_id", fmt.Sprintf("%d", productID))
	values.Add("email", email)
	values.Add("system_uuid", systemUUID)
	values.Add("system_os", systemOS)
	values.Add("system_os_version", systemOSVersion)
	values.Add("system_os_build", systemOSBuild)
	values.Add("system_app_version", systemAppVersion)
	values.Add("rating", fmt.Sprintf("%d", rating))
	return c.handleBoolResponse("growkit/rating", values)
}

// MakeRequest sends a payload to the SDK API and interprets the response.
func (c *Client) makeRequest(path string, values url.Values, responseFormat interface{}) ([]byte, error) {
	// Add the API key to the POST values.
	values.Add("api_key", c.Key)

	// Make the request to the SDK API.
	resp, err := c.HTTPClient.PostForm(c.buildURL(path), values)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// Read in the entire body.
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	// Decode the response body.
	if err = json.Unmarshal(body, responseFormat); err != nil {
		return body, err
	}

	return body, nil
}

func (c *Client) handleBoolResponse(path string, values url.Values) (bool, *APIError, error) {
	var resp apiResponse
	if _, err := c.makeRequest(path, values, &resp); err != nil {
		return false, nil, err
	}
	if resp.Err != nil {
		return false, resp.Err, nil
	}
	return resp.Success, nil, nil
}

func (c *Client) buildURL(path string) string {
	if c.BaseURL[len(c.BaseURL)-1] != '/' {
		c.BaseURL += "/"
	}
	path = strings.TrimLeft(path, "/")
	return c.BaseURL + path
}

func priceToFloat(p interface{}) (float64, error) {
	// Is the price already a float?
	pf, ok := p.(float64)
	if ok {
		return pf, nil
	}

	// Is the price a float wrapped in a string?
	ps, ok := p.(string)
	if ok {
		pf, err := strconv.ParseFloat(ps, 64)
		if err == nil {
			return pf, nil
		}
	}

	return 0, errors.New("Price returned from API is not a number")
}

func idToUint(id interface{}) (uint, error) {
	// Is the id a valid number?
	idf, ok := id.(float64)
	if ok {
		return uint(idf), nil
	}

	// Is the price a uint wrapped in a string?
	ids, ok := id.(string)
	if ok {
		idu, err := strconv.ParseUint(ids, 10, 32)
		if err == nil {
			return uint(idu), nil
		}
	}

	return 0, errors.New("ID returned from API is not a number")
}

func convertPricesToFloat(ps map[string]interface{}) (map[string]float64, error) {
	rs := make(map[string]float64)
	for key, val := range ps {
		r, err := priceToFloat(val)
		if err != nil {
			return nil, err
		}
		rs[key] = r
	}
	return rs, nil
}
